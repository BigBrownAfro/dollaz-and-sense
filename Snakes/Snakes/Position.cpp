#include "Position.h"


Position::Position() {
	x = 19;
	y = 19;
}

Position::Position(int x, int y):x(x), y(y)
{

}
//Overload for the position assignment operator
Position& Position::operator=(const Position& p) { x = p.x; y = p.y; return *this; }


Position::~Position()
{
	cout << "position destroyed";
}
