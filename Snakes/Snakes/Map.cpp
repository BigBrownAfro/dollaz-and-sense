#include "Map.h"
#include <cstdlib>
#include <ctime>
#include <string>
#include <fstream>
#include <iostream>


Map::Map(int players)
{
	isGameTime = false;

	//Add player snakes into array
	for (int i = 0; i < players; i++) {
		Snake *snake = new Snake(i + 1);
		snakes.push_back(snake);
	}

	//initialize map
	for (int i = 0; i < MAP_SIZE; i++) { //row
		for (int j = 0; j < MAP_SIZE; j++) { //col
			tiles[j][i] = Tile::empty;
		}
	}

}

void Map::spawnPowerUp() {
	bool isValid = false;
	powerUpTimeCounter = 0;
	while (!isValid) {
		srand(time(NULL)); //set seed based on current millis
		int xRand = (int)((rand()) % (MAP_SIZE - 1));
		int yRand = (int)((rand()) % (MAP_SIZE - 1));

		//check spot to make sure it's empty
		if (tiles[xRand][yRand] == Tile::empty) {
			tiles[xRand][yRand] = Tile::powerUp;
			isValid = true;
		}
		else {
			//cout << "Invalid powerup spawn" << endl;
		}
	}
}

void Map::start() {
	isGameTime = true;

// Here we use threading, a concept not shown in class
	mapPrinter = thread(&Map::printMap, this);

	while (isGameTime) {
		//update logic
		update();
		if (powerUpTimeCounter == 10) {
			spawnPowerUp();
		}
		powerUpTimeCounter++;
		//check game over condition
		int snakesAlive = 0;
		for (int i = 0; i < snakes.size(); i++) {
			if ((*snakes.at(i)).isAlive) {
				snakesAlive += 1;
			}
		}
		if (snakesAlive <= 1) {
			isGameTime = false;
			string name;
			cout << "Please enter your name" << endl;
			cin >> name;

			//Exception Handling when trying to open a file
			ofstream fout;
			try {
				fout.open("leaderboard.txt", ofstream::app); //opens leaderboard.txt in append mode
			}
			catch (exception) {
				cerr << "Failed to open leaderboard.txt";
				exit(69);
			}

			fout << (*snakes.at(0)).bodyPositions.size() << "\t" << name << endl;

			cout << "Your score is " << (*snakes.at(0)).bodyPositions.size() << " \nThe leaderboards have been updated\n";

		}

		//delay half a second
		Sleep(250);
	}
};

void Map::update() {
	//Move Snakes
	for (int i = 0; i < snakes.size(); i++) {
		if ((*snakes.at(i)).isAlive) {
			(*snakes.at(i)).move();
		}
	}

	//Check for collisions
	for (int i = 0; i < snakes.size(); i++) {
		if ((*snakes.at(i)).isAlive) {
			for (int j = 0; j < snakes.size(); j++) {
				for (int k = 0; k < (*snakes.at(j)).bodyPositions.size(); k++) {	
						if ((*snakes.at(i)).headPosition.x == (*snakes.at(j)).bodyPositions.at(k).x && (*snakes.at(i)).headPosition.y == (*snakes.at(j)).bodyPositions.at(k).y) {
							(*snakes.at(i)).kill();
						}
					}
			
			}
		}
	}



	//Check for powerUp collision
	for (int i = 0; i < snakes.size(); i++) {
		if (tiles[(*snakes.at(i)).headPosition.x][(*snakes.at(i)).headPosition.y] == powerUp) {
			Position* position = new Position((*snakes.at(i)).headPosition);
			(*snakes.at(i)).bodyPositions.push_back(*position);
			//cout << "PowerUp Picked Up";
		}
	}
	//Assign tiles to map
	for (int i = 0; i < MAP_SIZE; i++) { //row
		for (int j = 0; j < MAP_SIZE; j++) { //col
			if (tiles[j][i] != Tile::powerUp) {
				tiles[j][i] = Tile::empty; //clear tiles
			}

		}
	}
	
	for (int i = 0; i < snakes.size(); i++) {
		if ((*snakes.at(i)).isAlive) {
			tiles[(*snakes.at(i)).headPosition.x][(*snakes.at(i)).headPosition.y] = Tile::head; //assign heads
			for (int j = 0; j < (*snakes.at(i)).bodyPositions.size(); j++) {
				tiles[(*snakes.at(i)).bodyPositions.at(j).x][(*snakes.at(i)).bodyPositions.at(j).y] = Tile::body;
			}
		}
		
		
	}
}

void Map::printMap() {
	//Make a string that will be used to build the map
	string sMap = "";

	while (isGameTime) {
		//spacers
		sMap += string(2, '\n');

		for (int i = 0; i < MAP_SIZE; i++) { //row
			for (int j = 0; j < MAP_SIZE; j++) { //col
				//Make a char that will be drawn to screen
				char c = ' ';

				//decide what the char will be based on what's on the maptile
				switch (tiles[j][i]) {
				case Tile::head:
					c = 'O';
					break;
				case Tile::body:
					c = 'o';
					break;
				case Tile::powerUp:
					c = '/';
					break;
				case Tile::empty:
					c = '*';
					break;
				}

				//draw the char string map builder
				sMap += string(1, c) + " ";
			}
			//next row of map tiles
			sMap += "\n";
		}
		//Draw the string map builder to the screen
		cout << sMap;

		//Empty the map builder
		sMap = "";
		Sleep(120);
	}
}

Map::~Map()
{
	mapPrinter.join();
	cout << "Map destoyed along with threads" << endl;
}
