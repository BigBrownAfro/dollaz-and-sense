/*
Elijah Williams and Jacob Jackson
CS 3150
Final Project
Snakes Command Line Game

Manual:
	Game Description:
		The project is a two player snake command line game.
		The goal for each player is to move their snake around the screen collecting
		powerups that lengthen their snake. Should a snake's head run into a snake's
		body (theirs or the other player's) that snake will die.
		Last player alive wins.
		Their are no walls so the snakes can wrap across the map.

	Controls:
		Player1:
			W -> Move Up
			A -> Move Left
			S -> Move Down
			D -> Move Right

		Player2:
			Up-Arrow -> Move Up
			Left-Arrow -> Move Left
			Down-Arrow -> Move Down
			Right-Arrow -> Move Right

Project Requirements from Submission Page:
	Create an application in C++ complex enough to demonstrate the majority of presented concepts.
		The application holds concepts like references, pointers, vectors, operator overloading, classes, composition, etc.

	Project must demonstrate basic concepts. 
		Duh...

	Project must demonstrate object oriented programming. 
		We have Objects for Snakes, Positions, and Map.

	Project must demonstration operator overloading with a unique class type.
		In the Position class we overload the assignment operator.
		The reason was it makes it easy to shift the snake's body positions up
		if we can just run through the array and assign the positions to the ones
		above them.

	Project must include C++ exception handling.
		Exception handling required when working with file io
		Map.cc, line 73

	Project must demonstrate a concept not reviewed in class. 
		We use multiple threads in our project so things can happen simultaneously
		without relying on eachother.
		Different threads: Map Drawing thread (Map), Game Logic Updater (Map) (this is the original thread), and each Snake has a control listening thread.
*/

#include <iostream>
#include <fstream>
#include "Snake.h"
#include "Map.h"

using namespace std;

int main() {

	//Start the game with two players
	Map game(2);
	game.start();

	//Prints when game is over for fun
	cout << "Hiss" << endl;
}