#pragma once

#include "Position.h"
#include <vector>
#include <thread>
#include <Windows.h>

using namespace std;

const int MAP_SIZE = 40;

enum Move {
	up,
	down,
	left,
	right
};

//Object oriented programming 
class Snake
{
private:
	char upKey;
	char downKey;
	char leftKey;
	char rightKey;
	Move nextMovement;
	thread inputListener;

	void listenForInput();

public:
	bool isAlive;
	Position headPosition;
	vector<Position> bodyPositions;

	Snake();
	Snake(int);
	void kill();
	void move();
	~Snake();
};

