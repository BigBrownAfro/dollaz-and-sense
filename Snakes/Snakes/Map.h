#pragma once

#include <vector>
#include "Snake.h"

using namespace std;

enum Tile {
	head,
	body,
	powerUp,
	empty
};


class Map
{
private:
	bool isGameTime;
	vector<Snake*> snakes;
	Tile tiles[MAP_SIZE][MAP_SIZE];
	thread mapPrinter;
	int powerUpTimeCounter;

	void spawnPowerUp();
	void update();
	void printMap();
	
public:
	Map(int);
	void start();
	~Map();
};

