#include "Snake.h"

Snake::Snake()
	:isAlive(true), nextMovement(Move::up), headPosition(15,15)
{
	upKey = 'W';
	downKey = 'S';
	leftKey = 'A';
	rightKey = 'D';
}

Snake::Snake(int playerNumber)
	:isAlive(true), nextMovement(Move::down), headPosition(3 + playerNumber, 3 + playerNumber)
{
	if (playerNumber == 1) {
		upKey = 'W';
		downKey = 'S';
		leftKey = 'A';
		rightKey = 'D';
	}
	else {
		upKey = 0x26; //Up Arrow
		downKey = 0x28; //Down Arrow
		leftKey = 0x25; //Left Arrow
		rightKey = 0x27; //Right Arrow
	}

	inputListener = thread(&Snake::listenForInput, this);
}

void Snake::listenForInput() {
	while (isAlive) {
		if (GetKeyState(upKey) & 0b1000000000000000) { //checks to see if the highest bit is set (key is pressed) using bitwise operator
			if(nextMovement != Move::down)
			nextMovement = Move::up;
		}
		if (GetKeyState(downKey) & 0x8000) { // Same check for highest bit but prettier using hex
			if (nextMovement != Move::up)
			nextMovement = Move::down;
		}
		if (GetKeyState(leftKey) & 0x8000) {
			if (nextMovement != Move::right)
			nextMovement = Move::left;
		}
		if (GetKeyState(rightKey) & 0x8000) {
			if (nextMovement != Move::left)
			nextMovement = Move::right;
		}
		Sleep(30);
	}
}

void Snake::kill() {
	isAlive = false;
}

void Snake::move() {
	//Here we use the positions overloaded assignment operator
	if (bodyPositions.size() != 0) {
		for (int i = bodyPositions.size() - 1; i > 0; i--) {
			bodyPositions.at(i) = bodyPositions.at(i - 1);
		}
		bodyPositions.at(0) = headPosition;
	}
	else {
		Position* position = new Position(headPosition);
		bodyPositions.push_back(*position);
		bodyPositions.at(0) = headPosition;
	}
	

	switch (nextMovement) {
	case Move::up:
		if (headPosition.y - 1 < 0) {
			headPosition.y = MAP_SIZE - 1;
		}
		else {
			headPosition.y -= 1;
		}
		break;
	case Move::down:
		if (headPosition.y + 1 > MAP_SIZE - 1) {
			headPosition.y = 0;
		}
		else {
			headPosition.y += 1;
		}
		break;
	case Move::left:
		if (headPosition.x - 1 < 0) {
			headPosition.x = MAP_SIZE - 1;
		}
		else {
			headPosition.x -= 1;
		}
		break;
	case Move::right:
		if (headPosition.x + 1 > MAP_SIZE - 1) {
			headPosition.x = 0;
		}
		else {
			headPosition.x += 1;
		}
		break;
	}
}

Snake::~Snake()
{
	isAlive = false;
	inputListener.join();
	cout << "Snake destructed along with thread" << endl;
}
