#pragma once

#include <iostream>

using namespace std;

class Position
{
public:
	int x;
	int y;

	Position();
	Position(int, int);
	~Position();
	Position& operator=(const Position&);
};



